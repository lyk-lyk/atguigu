module.exports = {
  query(sql, params = null) {
    return new Promise((resolve, reject) => {
      const mysql = require("mysql");
      const connection = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "root",
        database: "tour",
      });

      connection.connect((err) => {
        if (err) Promise.reject(err);
      });

      connection.query(sql, params, (err, results, fileds) => {
        if (err) return reject(err.message);
        resolve({
          results,
          fileds,
        });
      });

      connection.end((err) => {
        if (err) Promise.reject(err);
      });
    });
  },
};
