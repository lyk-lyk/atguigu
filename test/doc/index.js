/** 1.1 请求所有的资源
  * @api {get} /users 1. 请求所有的资源
  * @apiName GetUser
  * @apiGroup User
  *
  * @apiParam {null} null null
  *
  * @apiSuccessExample {json} Success-Response:
  {
    "code": 200,
    "message": [
        {
            "id": 1,
            "user_name": "111112",
            "user_phone": "13126919251",
            "user_pwd": "123456",
            "user_headpic": "https://tse3-mm.cn.bing.net/th/id/OIP-C.QFdwl07_aviM1ch2KpyyFgHaEo?pid=ImgDet&rs=1",
            "user_sex": 1
        },
        {
            "id": 2,
            "user_name": "柳生飘絮",
            "user_phone": "13126919255",
            "user_pwd": "123456",
            "user_headpic": "https://tse3-mm.cn.bing.net/th/id/OIP-C.QFdwl07_aviM1ch2KpyyFgHaEo?pid=ImgDet&rs=1",
            "user_sex": 0
        },
      ]
    }
  */

/** 1.2 请求指定id资源
  * @api {get} /users/:id 1. 请求指定id资源
  * @apiName getUsersById
  * @apiGroup User
  *
  * @apiParam {number} id 用户id
  *
  * @apiSuccessExample {json} Success-Response:
  {
    "code": 200,
    "message": [
        {
            "id": 1,
            "user_name": "111112",
            "user_phone": "13126919251",
            "user_pwd": "123456",
            "user_headpic": "https://tse3-mm.cn.bing.net/th/id/OIP-C.QFdwl07_aviM1ch2KpyyFgHaEo?pid=ImgDet&rs=1",
            "user_sex": 1
        }
      ]
    }
  */

/** 1.3 注册用户
  * @api {post} /users 1. 注册用户
  * @apiName addUsers
  * @apiGroup User
  *
  * @apiParam {string} id 用户id
  * @apiParam {string} user_name user_name
  * @apiParam {string} user_phone user_phone
  * @apiParam {string} user_pwd user_pwd
  * @apiParam {string} user_headpic user_headpic
  * @apiParam {number} user_sex user_sex
  *
  * @apiSuccessExample {json} Success-Response:
  {
    "code": 200,
    "message": "success"
    }
  */

/** 1.4 更新用户信息
  * @api {patch} /users/:id 1. 更新用户信息
  * @apiName updateUsers
  * @apiGroup User
  *
  * @apiParam {string} id 用户id
  * @apiParam {string} user_name user_name
  * 接任意参数
  *
  * @apiSuccessExample {json} Success-Response:
  {
    "code": 200,
    "message": "success"
    }
  */

/** 1.5 删除用户
  * @api {delete} /users/:id 1. 删除用户
  * @apiName deleteUsers
  * @apiGroup User
  *
  * @apiParam {string} id 用户id
  *
  * @apiSuccessExample {json} Success-Response:
  {
    "code": 200,
    "message": "success"
    }
  */

/** 1.6 获取所有评论
  * @api {get} /cment 1. 获取所有评论
  * @apiName getComt
  * @apiGroup User
  *
  * @apiParam {null} null null
  *
  * @apiSuccessExample {json} Success-Response:
  {
    "code": 200,
    "message": [
        {
            "id": 1,
            "comment_content": "优惠券很好用，优惠力度很大",
            "comment_star": 5,
            "comment_user_phone": "13126919252",
            "comment_time": "1506605105000",
            "comment_coupon_id": 1
        },
        {
            "id": 2,
            "comment_content": "非常喜欢，产品不错",
            "comment_star": 4,
            "comment_user_phone": "13126919251",
            "comment_time": "1506605439000",
            "comment_coupon_id": 1
        },
      ]
    }
  */

/** 1.7 根据id获取评论
  * @api {get} /cment/:id 1. 根据id获取评论
  * @apiName getComtById
  * @apiGroup User
  *
  * @apiParam {string} comment_coupon_id comment_coupon_id
  *
  * @apiSuccessExample {json} Success-Response:
  {
    "code": 200,
    "message": [
        {
            "id": 1,
            "comment_content": "优惠券很好用，优惠力度很大",
            "comment_star": 5,
            "comment_user_phone": "13126919252",
            "comment_time": "1506605105000",
            "comment_coupon_id": 1
        }
      ]
    }
  */

/** 1.8 添加评论
  * @api {post} /  添加评论
  * @apiName addCment
  * @apiGroup User
  *
  * @apiParam {string} comment_content comment_content
  * @apiParam {string} conment_star conment_star
  * @apiParam {string} conment_user_phone conment_user_phone
  * @apiParam {string} conment_time 评论添加时间
  * @apiParam {string} conment_coupon_id 评论的商品的id
  * 
  * @apiSuccessExample {json} Success-Response:
   {
      code: 0,
      message: "success",
    }
  */

/** 1.9 获取所有商品信息
  * @api {get} /pro  获取所有商品信息
  * @apiName getComt
  * @apiGroup User
  *
  * @apiParam {null} null null
  * 
  * @apiSuccessExample {json} Success-Response:
   {
    "code": 200,
    "message": [
        {
            "id": 3,
            "coupon_name": "新罗免税店",
            "coupon_explain": "满2000立减100元",
            "coupon_starttime": "1506524754000",
            "coupon_endtime": "1514300798000",
            "coupon_recived_num": "6",
            "coupon_detail": " 这是是详细介绍 ",
            "coupon_belong_country": 3,
            "coupon_type": "union",
            "coupon_ico_path": "https://img.zcool.cn/community/01ca015d6a2950a801211f9ee98f02.jpg@2o.jpg",
            "coupon_status": "0",
            "coupon_classify": 1
        },
      ]
    }
  */

/** 1.10 根据id获取商品信息
  * @api {get} /pro/:id  根据id获取商品信息
  * @apiName getComtById
  * @apiGroup User
  *
  * @apiParam {number} id comment_coupon_id
  * 
  * @apiSuccessExample {json} Success-Response:
   {
    "code": 200,
    "message": [
        {
            "id": 3,
            "coupon_name": "新罗免税店",
            "coupon_explain": "满2000立减100元",
            "coupon_starttime": "1506524754000",
            "coupon_endtime": "1514300798000",
            "coupon_recived_num": "6",
            "coupon_detail": " 这是是详细介绍 ",
            "coupon_belong_country": 3,
            "coupon_type": "union",
            "coupon_ico_path": "https://img.zcool.cn/community/01ca015d6a2950a801211f9ee98f02.jpg@2o.jpg",
            "coupon_status": "0",
            "coupon_classify": 1
        },
      ]
    }
  */

/** 1.11 根据coupon_status获取商品信息
  * @api {get} /users/ctry/:id  根据coupon_status获取商品信息
  * @apiName getCtryById
  * @apiGroup User
  *
  * @apiParam {number} id coupon_status
  * 
  * @apiSuccessExample {json} Success-Response:
   {
    "code": 200,
    "message": [
        {
            "id": 3,
            "coupon_name": "新罗免税店",
            "coupon_explain": "满2000立减100元",
            "coupon_starttime": "1506524754000",
            "coupon_endtime": "1514300798000",
            "coupon_recived_num": "6",
            "coupon_detail": " 这是是详细介绍 ",
            "coupon_belong_country": 3,
            "coupon_type": "union",
            "coupon_ico_path": "https://img.zcool.cn/community/01ca015d6a2950a801211f9ee98f02.jpg@2o.jpg",
            "coupon_status": "0",
            "coupon_classify": 1
        },
        {
            "id": 4,
            "coupon_name": "都塔购物中心",
            "coupon_explain": "满2000立减100元",
            "coupon_starttime": "1506524754000",
            "coupon_endtime": "1514300798000",
            "coupon_recived_num": "10",
            "coupon_detail": " 这是是详细介绍 ",
            "coupon_belong_country": 3,
            "coupon_type": "union",
            "coupon_ico_path": "https://ts1.cn.mm.bing.net/th/id/R-C.baf4df6877acb863fc1898445509b11b?rik=8FCeHAAeFOCekA&riu=http%3a%2f%2fwww.gaobei.com%2fupload%2f10001%2farticle%2f2018_12%2f30220213_ise672.jpg&ehk=enptBUZ2eLPEhSZzCdFtS1DjvRoYFXqif9R%2fNj4ojF4%3d&risl=&pid=ImgRaw&r=0",
            "coupon_status": "0",
            "coupon_classify": 1
        },
      ]
    }
  */

/** 1.12 根据id获取优惠卷
  * @api {get} /users/status/:id  根据id获取优惠卷
  * @apiName getStatusById
  * @apiGroup User
  *
  * @apiParam {number} id user_id
  * 
  * @apiSuccessExample {json} Success-Response:
   {
    "code": 200,
    "message": [
        {
            "id": 3,
            "coupon_name": "新罗免税店",
            "coupon_explain": "满2000立减100元",
            "coupon_starttime": "1506524754000",
            "coupon_endtime": "1514300798000",
            "coupon_recived_num": "6",
            "coupon_detail": " 这是是详细介绍 ",
            "coupon_belong_country": 3,
            "coupon_type": "union",
            "coupon_ico_path": "https://img.zcool.cn/community/01ca015d6a2950a801211f9ee98f02.jpg@2o.jpg",
            "coupon_status": "0",
            "coupon_classify": 1
        },
        {
            "id": 4,
            "coupon_name": "都塔购物中心",
            "coupon_explain": "满2000立减100元",
            "coupon_starttime": "1506524754000",
            "coupon_endtime": "1514300798000",
            "coupon_recived_num": "10",
            "coupon_detail": " 这是是详细介绍 ",
            "coupon_belong_country": 3,
            "coupon_type": "union",
            "coupon_ico_path": "https://ts1.cn.mm.bing.net/th/id/R-C.baf4df6877acb863fc1898445509b11b?rik=8FCeHAAeFOCekA&riu=http%3a%2f%2fwww.gaobei.com%2fupload%2f10001%2farticle%2f2018_12%2f30220213_ise672.jpg&ehk=enptBUZ2eLPEhSZzCdFtS1DjvRoYFXqif9R%2fNj4ojF4%3d&risl=&pid=ImgRaw&r=0",
            "coupon_status": "0",
            "coupon_classify": 1
        },
      ]
    }
  */

/** 1.13 根据id获取info
  * @api {get} /info/:id  根据id获取info
  * @apiName getInfoById
  * @apiGroup User
  *
  * @apiParam {number} id user_id
  * 
  * @apiSuccessExample {json} Success-Response:
   {
    "code": 200,
    "message": [
        {
          "coupon_name": "新罗免税店",
          "coupon_starttime": "1506524754000",
          "coupon_endtime": "1514300798000",
          "coupon_explain": "满2000立减100元",
          "coupon_ico_path": "https://img.zcool.cn/community/01ca015d6a2950a801211f9ee98f02.jpg@2o.jpg"
        }
      ]
    }
  */

/** 1.14 提交选定的优惠卷
  * @api {post} /users/status  提交选定的优惠卷
  * @apiName addStatus
  * @apiGroup User
  *
  * @apiParam {number} id user_id
  * @apiParam {number} id coupon_id
  * @apiParam {number} id status
  * 
  * @apiSuccessExample {json} Success-Response:
   {
    "code": 200,
    "message": [
        {
          "coupon_name": "新罗免税店",
          "coupon_starttime": "1506524754000",
          "coupon_endtime": "1514300798000",
          "coupon_explain": "满2000立减100元",
          "coupon_ico_path": "https://img.zcool.cn/community/01ca015d6a2950a801211f9ee98f02.jpg@2o.jpg"
        }
      ]
    }
  */
