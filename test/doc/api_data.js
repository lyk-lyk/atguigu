define({ "api": [
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "doc/main.js",
    "group": "C:\\Users\\Administrator\\Desktop\\网络项目\\test\\doc\\main.js",
    "groupTitle": "C:\\Users\\Administrator\\Desktop\\网络项目\\test\\doc\\main.js",
    "name": ""
  },
  {
    "type": "get",
    "url": "/users",
    "title": "1. 请求所有的资源",
    "name": "GetUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "null",
            "optional": false,
            "field": "null",
            "description": "<p>null</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"code\": 200,\n  \"message\": [\n      {\n          \"id\": 1,\n          \"user_name\": \"111112\",\n          \"user_phone\": \"13126919251\",\n          \"user_pwd\": \"123456\",\n          \"user_headpic\": \"https://tse3-mm.cn.bing.net/th/id/OIP-C.QFdwl07_aviM1ch2KpyyFgHaEo?pid=ImgDet&rs=1\",\n          \"user_sex\": 1\n      },\n      {\n          \"id\": 2,\n          \"user_name\": \"柳生飘絮\",\n          \"user_phone\": \"13126919255\",\n          \"user_pwd\": \"123456\",\n          \"user_headpic\": \"https://tse3-mm.cn.bing.net/th/id/OIP-C.QFdwl07_aviM1ch2KpyyFgHaEo?pid=ImgDet&rs=1\",\n          \"user_sex\": 0\n      },\n    ]\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "doc/index.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/",
    "title": "添加评论",
    "name": "addCment",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "comment_content",
            "description": "<p>comment_content</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "conment_star",
            "description": "<p>conment_star</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "conment_user_phone",
            "description": "<p>conment_user_phone</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "conment_time",
            "description": "<p>评论添加时间</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "conment_coupon_id",
            "description": "<p>评论的商品的id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   code: 0,\n   message: \"success\",\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "doc/index.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/users/status",
    "title": "提交选定的优惠卷",
    "name": "addStatus",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "id",
            "description": "<p>user_id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n \"code\": 200,\n \"message\": [\n     {\n       \"coupon_name\": \"新罗免税店\",\n       \"coupon_starttime\": \"1506524754000\",\n       \"coupon_endtime\": \"1514300798000\",\n       \"coupon_explain\": \"满2000立减100元\",\n       \"coupon_ico_path\": \"https://img.zcool.cn/community/01ca015d6a2950a801211f9ee98f02.jpg@2o.jpg\"\n     }\n   ]\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "doc/index.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/users",
    "title": "1. 注册用户",
    "name": "addUsers",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_name",
            "description": "<p>user_name</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_phone",
            "description": "<p>user_phone</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_pwd",
            "description": "<p>user_pwd</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_headpic",
            "description": "<p>user_headpic</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "user_sex",
            "description": "<p>user_sex</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"code\": 200,\n  \"message\": \"success\"\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "doc/index.js",
    "groupTitle": "User"
  },
  {
    "type": "delete",
    "url": "/users/:id",
    "title": "1. 删除用户",
    "name": "deleteUsers",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>用户id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"code\": 200,\n  \"message\": \"success\"\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "doc/index.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/cment",
    "title": "1. 获取所有评论",
    "name": "getComt",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "null",
            "optional": false,
            "field": "null",
            "description": "<p>null</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"code\": 200,\n  \"message\": [\n      {\n          \"id\": 1,\n          \"comment_content\": \"优惠券很好用，优惠力度很大\",\n          \"comment_star\": 5,\n          \"comment_user_phone\": \"13126919252\",\n          \"comment_time\": \"1506605105000\",\n          \"comment_coupon_id\": 1\n      },\n      {\n          \"id\": 2,\n          \"comment_content\": \"非常喜欢，产品不错\",\n          \"comment_star\": 4,\n          \"comment_user_phone\": \"13126919251\",\n          \"comment_time\": \"1506605439000\",\n          \"comment_coupon_id\": 1\n      },\n    ]\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "doc/index.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/pro",
    "title": "获取所有商品信息",
    "name": "getComt",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "null",
            "optional": false,
            "field": "null",
            "description": "<p>null</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n \"code\": 200,\n \"message\": [\n     {\n         \"id\": 3,\n         \"coupon_name\": \"新罗免税店\",\n         \"coupon_explain\": \"满2000立减100元\",\n         \"coupon_starttime\": \"1506524754000\",\n         \"coupon_endtime\": \"1514300798000\",\n         \"coupon_recived_num\": \"6\",\n         \"coupon_detail\": \" 这是是详细介绍 \",\n         \"coupon_belong_country\": 3,\n         \"coupon_type\": \"union\",\n         \"coupon_ico_path\": \"https://img.zcool.cn/community/01ca015d6a2950a801211f9ee98f02.jpg@2o.jpg\",\n         \"coupon_status\": \"0\",\n         \"coupon_classify\": 1\n     },\n   ]\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "doc/index.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/cment/:id",
    "title": "1. 根据id获取评论",
    "name": "getComtById",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "comment_coupon_id",
            "description": "<p>comment_coupon_id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"code\": 200,\n  \"message\": [\n      {\n          \"id\": 1,\n          \"comment_content\": \"优惠券很好用，优惠力度很大\",\n          \"comment_star\": 5,\n          \"comment_user_phone\": \"13126919252\",\n          \"comment_time\": \"1506605105000\",\n          \"comment_coupon_id\": 1\n      }\n    ]\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "doc/index.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/pro/:id",
    "title": "根据id获取商品信息",
    "name": "getComtById",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "id",
            "description": "<p>comment_coupon_id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n \"code\": 200,\n \"message\": [\n     {\n         \"id\": 3,\n         \"coupon_name\": \"新罗免税店\",\n         \"coupon_explain\": \"满2000立减100元\",\n         \"coupon_starttime\": \"1506524754000\",\n         \"coupon_endtime\": \"1514300798000\",\n         \"coupon_recived_num\": \"6\",\n         \"coupon_detail\": \" 这是是详细介绍 \",\n         \"coupon_belong_country\": 3,\n         \"coupon_type\": \"union\",\n         \"coupon_ico_path\": \"https://img.zcool.cn/community/01ca015d6a2950a801211f9ee98f02.jpg@2o.jpg\",\n         \"coupon_status\": \"0\",\n         \"coupon_classify\": 1\n     },\n   ]\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "doc/index.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/users/ctry/:id",
    "title": "根据coupon_status获取商品信息",
    "name": "getCtryById",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "id",
            "description": "<p>coupon_status</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n \"code\": 200,\n \"message\": [\n     {\n         \"id\": 3,\n         \"coupon_name\": \"新罗免税店\",\n         \"coupon_explain\": \"满2000立减100元\",\n         \"coupon_starttime\": \"1506524754000\",\n         \"coupon_endtime\": \"1514300798000\",\n         \"coupon_recived_num\": \"6\",\n         \"coupon_detail\": \" 这是是详细介绍 \",\n         \"coupon_belong_country\": 3,\n         \"coupon_type\": \"union\",\n         \"coupon_ico_path\": \"https://img.zcool.cn/community/01ca015d6a2950a801211f9ee98f02.jpg@2o.jpg\",\n         \"coupon_status\": \"0\",\n         \"coupon_classify\": 1\n     },\n     {\n         \"id\": 4,\n         \"coupon_name\": \"都塔购物中心\",\n         \"coupon_explain\": \"满2000立减100元\",\n         \"coupon_starttime\": \"1506524754000\",\n         \"coupon_endtime\": \"1514300798000\",\n         \"coupon_recived_num\": \"10\",\n         \"coupon_detail\": \" 这是是详细介绍 \",\n         \"coupon_belong_country\": 3,\n         \"coupon_type\": \"union\",\n         \"coupon_ico_path\": \"https://ts1.cn.mm.bing.net/th/id/R-C.baf4df6877acb863fc1898445509b11b?rik=8FCeHAAeFOCekA&riu=http%3a%2f%2fwww.gaobei.com%2fupload%2f10001%2farticle%2f2018_12%2f30220213_ise672.jpg&ehk=enptBUZ2eLPEhSZzCdFtS1DjvRoYFXqif9R%2fNj4ojF4%3d&risl=&pid=ImgRaw&r=0\",\n         \"coupon_status\": \"0\",\n         \"coupon_classify\": 1\n     },\n   ]\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "doc/index.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/info/:id",
    "title": "根据id获取info",
    "name": "getInfoById",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "id",
            "description": "<p>user_id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n \"code\": 200,\n \"message\": [\n     {\n       \"coupon_name\": \"新罗免税店\",\n       \"coupon_starttime\": \"1506524754000\",\n       \"coupon_endtime\": \"1514300798000\",\n       \"coupon_explain\": \"满2000立减100元\",\n       \"coupon_ico_path\": \"https://img.zcool.cn/community/01ca015d6a2950a801211f9ee98f02.jpg@2o.jpg\"\n     }\n   ]\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "doc/index.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/users/status/:id",
    "title": "根据id获取优惠卷",
    "name": "getStatusById",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "id",
            "description": "<p>user_id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n \"code\": 200,\n \"message\": [\n     {\n         \"id\": 3,\n         \"coupon_name\": \"新罗免税店\",\n         \"coupon_explain\": \"满2000立减100元\",\n         \"coupon_starttime\": \"1506524754000\",\n         \"coupon_endtime\": \"1514300798000\",\n         \"coupon_recived_num\": \"6\",\n         \"coupon_detail\": \" 这是是详细介绍 \",\n         \"coupon_belong_country\": 3,\n         \"coupon_type\": \"union\",\n         \"coupon_ico_path\": \"https://img.zcool.cn/community/01ca015d6a2950a801211f9ee98f02.jpg@2o.jpg\",\n         \"coupon_status\": \"0\",\n         \"coupon_classify\": 1\n     },\n     {\n         \"id\": 4,\n         \"coupon_name\": \"都塔购物中心\",\n         \"coupon_explain\": \"满2000立减100元\",\n         \"coupon_starttime\": \"1506524754000\",\n         \"coupon_endtime\": \"1514300798000\",\n         \"coupon_recived_num\": \"10\",\n         \"coupon_detail\": \" 这是是详细介绍 \",\n         \"coupon_belong_country\": 3,\n         \"coupon_type\": \"union\",\n         \"coupon_ico_path\": \"https://ts1.cn.mm.bing.net/th/id/R-C.baf4df6877acb863fc1898445509b11b?rik=8FCeHAAeFOCekA&riu=http%3a%2f%2fwww.gaobei.com%2fupload%2f10001%2farticle%2f2018_12%2f30220213_ise672.jpg&ehk=enptBUZ2eLPEhSZzCdFtS1DjvRoYFXqif9R%2fNj4ojF4%3d&risl=&pid=ImgRaw&r=0\",\n         \"coupon_status\": \"0\",\n         \"coupon_classify\": 1\n     },\n   ]\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "doc/index.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/users/:id",
    "title": "1. 请求指定id资源",
    "name": "getUsersById",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "id",
            "description": "<p>用户id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"code\": 200,\n  \"message\": [\n      {\n          \"id\": 1,\n          \"user_name\": \"111112\",\n          \"user_phone\": \"13126919251\",\n          \"user_pwd\": \"123456\",\n          \"user_headpic\": \"https://tse3-mm.cn.bing.net/th/id/OIP-C.QFdwl07_aviM1ch2KpyyFgHaEo?pid=ImgDet&rs=1\",\n          \"user_sex\": 1\n      }\n    ]\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "doc/index.js",
    "groupTitle": "User"
  },
  {
    "type": "patch",
    "url": "/users/:id",
    "title": "1. 更新用户信息",
    "name": "updateUsers",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_name",
            "description": "<p>user_name 接任意参数</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"code\": 200,\n  \"message\": \"success\"\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "doc/index.js",
    "groupTitle": "User"
  }
] });
