var db = require("../db");
module.exports = {
  async getUsers(req, res) {
    const sql = "select * from tour_user";
    const { results } = await db.query(sql);
    console.log("获取用户");
    res.send({
      code: 200,
      message: results,
    });
  },
  async getStatusById(req, res) {
    let arr = [];
    const sql =
      "select coupon_id from tour_coupon_user where status=1 and user_id = ?";
    var { results } = await db.query(sql, req.params.id);
    if (results.length === 0) {
      return res.send({
        code: 648,
        message: "无数据",
      });
    }
    results.forEach((item) => {
      arr.push(item.coupon_id);
    });
    let str = arr.join(",");
    const sql2 = `select * from tour_coupon_copy where id in ( ${str} )`;
    var { results } = await db.query(sql2);
    console.log("通过id获取优惠卷");
    return res.send({
      code: 200,
      message: results,
    });
  },
  async getPro(req, res) {
    const sql = "select * from tour_coupon_copy";
    const { results } = await db.query(sql);
    console.log("获取商品");
    return res.send({
      code: 200,
      message: results,
    });
  },
  async getCtry(req, res) {
    const sql = "select * from tour_coupon";
    const { results } = await db.query(sql);
    console.log("获取城市");
    return res.send({
      code: 200,
      message: results,
    });
  },
  async getComt(req, res) {
    const sql = "select * from tour_comment";
    const { results } = await db.query(sql);
    console.log("获取评论");
    res.send({
      code: 200,
      message: results,
    });
  },
  async getUsersById(req, res, next) {
    const sql = "select * from tour_user where id=?";
    const { results } = await db.query(sql, req.params.id);
    console.log("通过id获取用户");
    if (results.length === 0)
      return res.send({
        code: 1,
        message: "该id用户不存在",
      });
    res.send({
      code: 0,
      message: results,
    });
  },
  async getCtryById(req, res, next) {
    var sql;
    const sql1 = "select * from tour_coupon_copy";
    const sql2 =
      "select * from tour_coupon_copy where coupon_belong_country = ?";
    if (req.params.id == 0) {
      sql = sql1;
    } else {
      sql = sql2;
    }
    const { results } = await db.query(sql, req.params.id);
    console.log("通过id获取城市");
    if (results.length === 0)
      return res.send({
        code: 1,
        message: "该country用户不存在",
      });
    res.send({
      code: 0,
      message: results,
    });
  },
  async getProById(req, res, next) {
    const sql = "select * from tour_coupon_copy where id=?";
    const { results } = await db.query(sql, req.params.id);
    console.log("通过id获取商品");
    if (results.length === 0)
      return res.send({
        code: 1,
        message: "商品不存在",
      });
    res.send({
      code: 0,
      message: results,
    });
  },
  async getComtById(req, res, next) {
    const sql = "select * from tour_comment where comment_coupon_id=? limit 5";
    const { results } = await db.query(sql, req.params.id);
    console.log("通过id获取评论");
    if (results.length === 0)
      return res.send({
        code: 1,
        message: "comment用户不存在",
      });
    res.send({
      code: 0,
      message: results,
    });
  },
  async getInfoById(req, res, next) {
    const sql = `select coupon_name,
      coupon_starttime,
      coupon_endtime,
      coupon_explain,
      coupon_ico_path
      from tour_coupon_copy where id=?`;
    const { results } = await db.query(sql, req.params.id);
    console.log("通过id获取信息");
    if (results.length === 0)
      return res.send({
        code: 1,
        message: "info用户不存在",
      });
    res.send({
      code: 0,
      message: results,
    });
  },
  async addCment(req, res, next) {
    const sql = "insert into tour_comment set ?";
    const body = req.body;
    const { results } = await db.query(sql, body);
    console.log("添加评论");
    if (results.affectedRows === 0)
      return res.send({
        code: 1,
        message: "添加评论失败",
      });
    res.send({
      code: 0,
      message: "添加成功",
    });
  },
  async addStatus(req, res, next) {
    const sql = "insert into tour_coupon_user set ?";
    const body = req.body;
    const { results } = await db.query(sql, body);
    console.log("添加优惠卷");
    if (results.affectedRows === 0) {
      return res.send({
        code: 1,
        message: "添加优惠卷失败",
      });
    } else {
      res.send({
        code: 0,
        message: "添加成功",
      });
    }
  },
  async addUsers(req, res, next) {
    const query = `select * from tour_user where user_phone=${req.body.user_phone}`;
    const results1 = await db.query(query);
    console.log(req.body.user_phone);
    if (results1.results.length !== 0) {
      return res.send({
        code: 328,
        message: "用户以存在",
      });
    }
    const sql = "insert into tour_user set ?";
    const body = req.body;
    const { results } = await db.query(sql, body);
    console.log("添加用户");
    if (results.affectedRows === 0)
      return res.send({
        code: 1,
        message: "添加用户失败",
      });
    res.send({
      code: 0,
      message: "注册成功",
    });
  },
  async updateUsers(req, res, next) {
    const sql = "update tour_user set ? where id = ?";
    const body = req.body;
    const { results } = await db.query(sql, [body, req.params.id]);
    console.log("更新用户信息");
    if (results.affectedRows === 0)
      return res.send({
        code: 1,
        message: "更新用户失败",
      });
    res.send({
      code: 0,
      message: "success",
    });
  },
  async deleteUsers(req, res, next) {
    const sql = "update users set  where id = ?";
    const { results } = await db.query(sql, req.params.id);
    console.log(删除用户);
    if (results.affectedRows !== 1)
      return res.send({
        code: 1,
        message: "删除用户失败",
      });
    res.send({
      code: 0,
      message: "success",
    });
  },
};
