module.exports = (app, express) => {
  var db = require("../db");
  var router = express.Router();
  var router2 = express.Router();
  var router3 = express.Router();
  var router4 = express.Router();
  var router5 = express.Router();
  var router6 = express.Router();
  var usersService = require("../service/index");
  var jwt = require("jsonwebtoken");

  router.get("/", usersService.getUsers);
  router2.get("/", usersService.getComt);
  router5.get("/:id", usersService.getStatusById);
  router3.get("/", usersService.getPro);
  router.get("/:id", usersService.getUsersById);
  router.post("/", usersService.addUsers);
  router.patch("/:id", usersService.updateUsers);
  router.delete("/:id", usersService.deleteUsers);
  router3.get("/:id", usersService.getProById);
  router4.get("/:id", usersService.getCtryById);
  router2.get("/:id", usersService.getComtById);
  router6.get("/:id", usersService.getInfoById);
  router2.post("/", usersService.addCment);
  router5.post("/", usersService.addStatus);

  app.post("/login", async (req, res) => {
    const sql = "select * from tour_user where user_phone = ?";
    const { results } = await db.query(sql, req.body.user_phone);
    if (results.length === 0) {
      return res.send({
        code: -0010,
        msg: "用户不存在",
      });
    }
    if (req.body.user_pwd !== results[0].user_pwd) {
      return res.send({
        code: -1101,
        msg: "密码错误,请重新输入",
      });
    }
    const token = jwt.sign(
      {
        data: "foobar",
      },
      "secret",
      { expiresIn: 1000 * 60 * 60 * 24 * 7 }
    );
    res.send({
      code: 0000,
      msg: {
        token,
        results,
      },
    });
  });

  // 用户路由
  app.use("/users", router);
  // 评论路由
  app.use("/cment", router2);
  // 获取信息的路由
  app.use("/info", router6);
  // 商品路由
  app.use("/pro", router3);
  // 城市路由
  app.use("/users/ctry", router4);
  // 优惠卷路由
  app.use("/users/status", router5);
};
