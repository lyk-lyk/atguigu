import request from '@/utils/request';

export const login = (data) => {
  return request({
    method: 'post',
    url: '/login',
    data,
  });
};
export const addUsers = (data) => {
  return request({
    method: 'post',
    url: '/users',
    data,
    // headers: { ['Content-Type']: 'x-www-form-urlencode' },
  });
};
export const updatePwd = (data, id) => {
  return request({
    method: 'patch',
    url: '/users/' + id,
    data,
  });
};

export const getComments = (id) => {
  return request({
    method: 'get',
    url: '/cment/' + id,
  });
};

export const getCountry = (id) => {
  return request({
    method: 'get',
    url: '/users/ctry/' + id,
  });
};

export const getCoupon = (id) => {
  return request({
    method: 'get',
    url: '/info/' + id,
  });
};

export const addComments = (data) => {
  return request({
    method: 'post',
    url: '/cment',
    data,
  });
};
export const getcouponID = (id) => {
  return request({
    method: 'get',
    url: '/users/status/' + id,
  });
};
export const usersNema = (data, id) => {
  return request({
    method: 'patch',
    url: '/users/' + id,
    data,
  });
};
export const addCoupon = (data) => {
  return request({
    method: 'post',
    url: '/users/status',
    data,
  });
};
