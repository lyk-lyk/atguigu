import reg from './reg';
import login from './login';
export default [
  {
    path: '/nelogin',
    redirect: '/login',
  },
  {
    path: '/nelogin',
    name: 'nelogin',
    component: () => import('@/views/views1/mypage/nelogin'),
    children: [...reg, ...login],
  },
];
