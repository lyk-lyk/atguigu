export default [
  {
    path: '/getCoupon/:id',
    component: () => import('@/views/dx/getcoupon/index.vue'),
    meta: {
      title: '领取优惠券',
      isShow: false,
    },
  },
  {
    path: '*',
    component: () => import('@/views/dx/404'),
  },
];
