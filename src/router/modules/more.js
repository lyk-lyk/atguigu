export default [
  {
    path: '/more',
    name: 'more',
    component: () => import('@/views/more/index.vue'),
  },
];
