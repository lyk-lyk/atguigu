export default [
  {
    path: '/data',
    name: 'data',
    component: () => import('@/views/data/index.vue'),
  },
];
