import Vue from 'vue';
import VueRouter from 'vue-router';
import home from './modules/home';
import mypage from './modules/routes1/nelogin';
import searchPsw from './modules/routes1/searchPsw';
import dx from './modules/dx';
import more from './modules/more';
import data from './modules/data';
import my from './modules/routes1/my';

Vue.use(VueRouter);
const routes = [...home, ...mypage, ...searchPsw, ...dx, ...more, ...data, ...my];
const router = new VueRouter({
  routes,
});

export default router;
