import axios from 'axios';
import { Toast } from 'vant';

const service = axios.create({
  baseURL: 'http://10.41.1.254:3000',
  timeout: 3000,
});

// 配置响应拦截
service.interceptors.response.use(
  (config) => {
    return config;
  },
  (err) => {
    Toast.fail('网络请求失败');
    Promise.reject(err);
  }
);

export default service;
