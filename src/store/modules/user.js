const state = {
  user: {},
};

const mutations = {
  addUser(state, user) {
    state.user = user;
    localStorage.user = JSON.stringify(state.user);
  },
  updateUser(state) {
    state.user = JSON.parse(localStorage.user || ' {}');
  },
};

const actions = {};

const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
