import router from '@/router'
const state = {
  obj1:{},
};

const mutations = {
  getCoupon(state,id) {
    // console.log(id);
    router.push({
      path: `/getcoupon/${id}`,
    });
  },
};

const actions = {};

const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
  watch:{
    obj1(){
      console.log(1);
    }
  }
};

// console.log(this.obj1);