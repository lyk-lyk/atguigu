const state = {};

const mutations = {
  returnBefore() {
    history.go(-1)
  },
};

const actions = {};

const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
