import { getCountry } from '@/api';
const state = {
  value1: 0,
  option1: [
    { text: '全球', value: 0 },
    { text: '港澳', value: 2 },
    { text: '韩国', value: 3 },
    { text: '日本', value: 4 },
    { text: '泰国', value: 5 },
    { text: '其他国家', value: 6 },
  ],
  array1: [],
}

const mutations = {
  async getCountry(id) {
    const res = await getCountry(id);
    state.array1 = res.data.message;
  },
  checkCountry(state,b) {
    state.value1=b
    mutations.getCountry(b);
    console.log(state);
  },
}

const actions = {

}

const getters = {
  getvalue1(state){
    return state.value1
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}