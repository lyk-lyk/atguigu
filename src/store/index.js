import Vue from 'vue';
import Vuex from 'vuex';
import user from './modules/user';
import returnBefore from './modules/returnBefore';
import checkCountry from './modules/checkCountry';
import getCoupon from './modules/getCoupon';
const modulesFiles = require.context('./modules', true, /\.js$/);

const modules = modulesFiles.keys().reduce((modules, modulesPath) => {
  const modulesName = modulesPath.replace(/^\.\/(.*)\.\w+$/, '$1');
  const value = modulesFiles(modulesPath);
  modules[modulesName] = value.default;
  return modules;
}, {});

Vue.use(Vuex);

export default new Vuex.Store({
  modules: { user, returnBefore, checkCountry, getCoupon },
});
