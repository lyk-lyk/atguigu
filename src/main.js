import Vue from 'vue';
import App from './App.vue';
import '@/assets/css/reset.css';
import '@/utils/request';
import router from '@/router';
import store from './store';
import '@/utils/request';
import Vant from 'vant';
import ElementUI from 'element-ui';
import './plugins/vant.js';
import 'vant/lib/index.css';
import moment from 'moment'
Vue.filter('dataFormat', function (time, format) {
  if (time) {
    return moment(time).format(format);
  }
});
Vue.use(Vant);
Vue.use(ElementUI);
import * as api from '@/api';
Vue.prototype.$http = api;

Vue.config.productionTip = false;
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');

router.beforeEach((to,from,next) => {
  if (to.path==="/login") {
    const token=localStorage.getItem("token")||''
    if (token!='') {
      return next("/my")
    }
    if(token==''){
      return next()
    }
  }else{
    return next()
  }
})





