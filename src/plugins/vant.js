import Vue from 'vue';
import Vant, { Lazyload } from 'vant';
import 'vant/lib/index.css';

Vue.use(Vant);
// 注册时可以配置额外的选项
Vue.use(Lazyload, {
  lazyComponent: true,
  loading:
    'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.zcool.cn%2Fcommunity%2F01f4055864c2c3a8012060c8dc7eca.gif&refer=http%3A%2F%2Fimg.zcool.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1666425265&t=ed65459dc823724e04800e673a823655',
});

