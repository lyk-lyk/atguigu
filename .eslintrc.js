module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    "plugin:vue/essential",
    "eslint:recommended",
    "plugin:prettier/recommended",
  ],
  parserOptions: {
    parser: "@babel/eslint-parser",
  },
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-unused-vars": 'off',
    // 关闭驼峰式命名
    "vue/multi-word-component-names": "off",
    "vue/return-in-computed-property":"off",
    // 关闭语法警告
    "prettier/prettier": [
      "off",
      {
        singleQuote: true,
      },
    ],
  },
};
